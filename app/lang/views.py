from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import StudentForm
from Quiz.models.language import Language
from Quiz.models.student import Student
import logging

def logout_view(request):
    logout(request)
    log = logging.getLogger(__name__)
    log.info("Sign-out: " + str(request.user.username))
    return render(request, 'registration/logged_out.html')

def new_login(request):
    log = logging.getLogger(__name__)
    log.info("Sign-in: " + str(request.user.username))
    return redirect('Quiz:dashboard')
 
def signup(request):
    log = logging.getLogger(__name__)
    if request.method == 'POST':
        user_form = UserCreationForm(request.POST)
        student_form = StudentForm(request.POST)
        if user_form.is_valid() and student_form.is_valid():
            user = user_form.save()
            username = user_form.cleaned_data.get('username')
            raw_password = user_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)

            student = Student.objects.create(user=user)
            student.native_languages.add(*student_form.cleaned_data.get('native_languages'))
            student.save()
#            student_form.save()  # Gracefully save the form
            log.info("New Sign-up: " + str(user.username))
            return redirect('Quiz:dashboard')
    else:
        user_form = UserCreationForm()
        student_form = StudentForm()
    return render(request, 'registration/signup.html', {
        'user_form': user_form,
        'student_form': student_form
    })