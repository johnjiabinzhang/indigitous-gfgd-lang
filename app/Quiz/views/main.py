from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from ..models import * #GramConcept, Stage, Grammar, Student, StudentProgress, Course, Question, GrammarProgress
from django.contrib import messages
from django.contrib.auth import logout
from common import css, template_filters
import logging
from django.contrib.auth.decorators import permission_required

@permission_required('quiz.admin')
@login_required
def reset_all(request):
    # delete student progress
    GrammarProgress.objects.all().delete()
    StudentProgress.objects.all().delete()
    
    # reset question scores    
    for q in Question.objects.all():
        q.variance = 250
        q.save() # resets question score to that of grammar
    
    # reset stage and grammar scores
    for s in Stage.objects.all():
        s.score = s.score
        s.save()
        for g in s.grammar_set.all():
            g.score = s.score
            g.save()

    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " executed reset_all")    
    logout(request)
    return redirect('Quiz:dashboard')


@login_required
def integrity_check(request,course_id=None):
    # checks state of course for any conditions which may cause a run-time error
    # check each grammar has at least one question
    problems = 0
    critical_problems = 0
    questions_exist = False
    if course_id == None:
        course_queryset = Course.objects.all()
    else:
        course_queryset = [get_object_or_404(Course, id=course_id)]
    for course in course_queryset:
        # check each course has stages
        if course.stage_set.exists():
            # check each stage has registered questions
            for stage in course.stage_set.all():
                if stage.grammar_set.exists():
                    for grammar in stage.grammar_set.all():
                        if grammar.question_set.exists():
                            questions_exist = True
                        else:
                            problems += 1
                            if request.user.has_perm('quiz.admin'):
                                messages.add_message(request, messages.WARNING, "Course:"+str(course)+" Stage:"+str(stage)+" Grammar:"+str(grammar)+" has no associated questions")                         
                else:
                    problems += 1
                    if request.user.has_perm('quiz.admin'):
                        messages.add_message(request, messages.WARNING, "Course:"+str(course)+" Stage:"+str(stage)+" has no registered grammars")
                    if stage.position == 1:
                        critical_problems += 1                                            
        else:
            messages.add_message(request, messages.WARNING, "Course:"+str(course)+" has no stages")   
            critical_problems += 1  
    if critical_problems == 0 and questions_exist == True:                    
        return redirect('Quiz:dashboard')
    else:
        messages.add_message(request, messages.INFO, "Try correcting above listed problems here, or use the Account menu to select a different course")
        return redirect('Quiz:gram-create')
     
@login_required
def dashboard(request):
    # check user has chosen which course they are studying today
    try:
        sp = StudentProgress.objects.get(id=request.session['sp_pk'])
    except:
        sps = StudentProgress.objects.filter(user=request.user)
        if sps.count() == 0:
            return redirect('Quiz:signup-course')     # if the student is not signed up to any courses, redirect    
        elif sps.count() == 1:
            request.session['choice'] = sps.first().course.id
            return redirect('Quiz:select-course')
        else:
            return redirect('Quiz:prep_select_course')

    if sp.initial_placement:
        messages.add_message(request, messages.INFO, "Since you have newly registered to this course, we will start you off with a quick initial assessment") 
        return redirect('Quiz:initial_placement_init')

    # determine progress in relevant stages
    stage_array={}
    relevant_stages = Stage.objects.filter(course=sp.course,position__lte=sp.stage.position)
        
    counter = 0
    for stg in relevant_stages:
        stage_array[counter]={'type':'stage','stage_titles':str(stg)}
        relevant_grammars = stg.grammar_set.all() #.order_by('score','title')
        counter += 1
        for grm in relevant_grammars:
            highlight = False
            gp = GrammarProgress.objects.filter(sp=sp, grammar= grm).first()
            if gp is None: # this shouldn't happen
                grammar_progress = 1
                colour = "Yellow" # untested
            elif gp.started == False:
                grammar_progress = 1
                colour = "Yellow" # untested
            else:
                if gp.id in css.to_list(sp.focuses):
                    highlight = True
                grammar_progress = gp.get_score()
                if gp.passing(sp.stage.position):
                    colour = "Green" # passing
                else:
                    colour = "Blue" # not passing

            stage_array[counter]={'type':"grammar",'grammar_progress':grammar_progress,'grammar_titles':grm.full_title(sp.native_language),'colour':colour, 'highlight':highlight}
            counter += 1
    context = {
        'stage_array':stage_array,
        'relevant_stages':relevant_stages,
    }
    return render(request, 'Quiz/dashboard.html', context)


@login_required
def prep_select_course(request):
    student = Student.objects.get(user=request.user)
    sps = StudentProgress.objects.filter(user=request.user)
    registered_courses = []
    for sp in sps:
        registered_courses.append({'id':sp.course.id, 'title':sp.course.title_in(sp.native_language)})
    context = {'registered_courses': registered_courses}
    return render(request, 'Quiz/select_course.html', context)    

@login_required
def select_course(request):
    if request.method == "POST":
        sp = get_object_or_404(StudentProgress, user=request.user, course = request.POST['choice'])
    else:
        sp = get_object_or_404(StudentProgress, user=request.user, course = request.session['choice'])
    request.session['course_pk'] = sp.course.id
    request.session['sp_pk'] = sp.id
    request.session['course_title'] = sp.course.title_in(sp.native_language)
    request.session['native_language_pk'] = sp.native_language.id
    log = logging.getLogger(__name__)
    log.info("User" + request.user.username + " selected course "+ str(sp.course))
    return redirect('Quiz:integrity_check', course_id=sp.course.id)

@login_required
def signup_course(request):
    # show all available courses. let them select one.
    student = get_object_or_404(Student, user=request.user)
    sp_ids = StudentProgress.objects.filter(user=request.user).values_list('course', flat=True)
    # exclude enrolled courses 
    unsigned_courses = Course.objects.exclude(id__in=sp_ids)
    # exclude courses not offered in their native_languages
    
    valid_courses = []
    for course in unsigned_courses:
        for nl in course.native_languages():
            if nl in student.native_languages.all():
                valid_courses.append({'course_id':course.id, 'title':course.title_in(nl), 'native_id':nl.id, 'native':nl.name, 'target':course.target_language.name})			
    context = {'valid_courses': valid_courses} 
    return render(request, 'Quiz/signup_course.html', context)

@login_required
def register_student_to_course(request):
# directed here from signup_course form
    [course_id,native_id] =request.POST['choice'].split(',')
    course = get_object_or_404(Course, id=course_id)
    native = get_object_or_404(Language, id=native_id) 
    sp = StudentProgress.objects.create(user=request.user, course=course, native_language=native)
    request.session['choice'] = sp.course.id
    return redirect('Quiz:select-course')
