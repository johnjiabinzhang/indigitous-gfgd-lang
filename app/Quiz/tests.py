import datetime
from django.utils import timezone
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from .models import Attempt, Comment, Course, Entry, GramConcept, GrammarProgress, Grammar, Language, Question, Sentence, Stage, StudentProgress, Student

class ViewTests(TestCase):
    pass

    def setUp(self):
        try:
            self.setup_done
        except:
            self.setup_done = True
            print('passed')
        else:
            print('failed')
            return
        
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='joe',email='joe@hotmail.com', password='qweqweqwe')
        self.u2 = User.objects.create_user(username='max',email='max@hotmail.com', password='qweqweqwe')
        self.u3 = User.objects.create_user(username='sally',email='sally@hotmail.com', password='qweqweqwe')
        self.l_EN = Language.objects.create(anglicized_name="English",name="English",iso693_2="eng",author=self.user)
        self.l_TR = Language.objects.create(anglicized_name="Turkish",name="Türkçe",iso693_2="tur",author=self.user)
        self.l_FR = Language.objects.create(anglicized_name="French",name="français",iso693_2="fre",author=self.user)
        self.l_RU = Language.objects.create(anglicized_name="Russian",name="русский",iso693_2="rus",author=self.user)
        self.st1 = Student.objects.create(user=self.user)
        self.st1.native_languages.add(self.l_EN)

        self.c1 = Course.objects.create(target_language=self.l_TR,author=self.user)
        self.sen_c1_tit = Sentence.objects.create(text="Learn Turkish_",language=self.l_EN,author=self.user)
        self.c1.titles.add(self.sen_c1_tit)

        self.c2 = Course.objects.create(target_language=self.l_RU,author=self.user)
        self.sen_c2_tit = Sentence.objects.create(text="Speak like a Russian",language=self.l_EN,author=self.user)
        self.c2.titles.add(self.sen_c2_tit)
        
        self.c3 = Course.objects.create(target_language=self.l_RU,author=self.user)
        self.sen_c3_tit = Sentence.objects.create(text="Rusça öğren",language=self.l_TR,author=self.user)
        self.c3.titles.add(self.sen_c3_tit)
        
        self.gc1 = GramConcept.objects.create(title='Plurals',author=self.user)
        self.g1_TR = Grammar.objects.create(gram_concept=self.gc1,target_language=self.l_TR,score=1000,author=self.user)
        self.g1_EN = Grammar.objects.create(gram_concept=self.gc1,target_language=self.l_EN,score=1000,author=self.user)

        self.sen_g1TR_tit = Sentence.objects.create(text="-ler/-lar suffix",language=self.l_EN,author=self.user)
        self.g1_TR.titles.add(self.sen_g1TR_tit) 

        self.sen_g1EN_tit = Sentence.objects.create(text="-s eki",language=self.l_TR,author=self.user)
        self.g1_EN.titles.add(self.sen_g1EN_tit) 
        
        self.sen1 = Sentence.objects.create(text="Books",language=self.l_EN,author=self.user)
        self.sen1.grammars.add(self.g1_EN)
        
        self.sen2 = Sentence.objects.create(text="Kitaplar",language=self.l_TR,author=self.user)
        self.sen2.grammars.add(self.g1_TR)

        self.q1 = Question.objects.create(front=self.sen1,back=self.sen2,target_language=self.l_TR,native_language=self.l_EN,grammar=self.g1_TR,author=self.user)
        
        for x in range(6):
            # create 6 random extra questions
            Question.objects.create(front=self.sen1,back=self.sen2,target_language=self.l_TR,native_language=self.l_EN,grammar=self.g1_TR,author=self.user)
                        
        self.sen_stage1EN_tit = Sentence.objects.create(text="Beginners",language=self.l_EN,author=self.user)
        self.sen_stage1FR_tit = Sentence.objects.create(text="Débutants",language=self.l_FR,author=self.user)
        
        self.stage1 = Stage.objects.get(position=1,course=self.c1) # automatically created when course was created
        self.stage1.titles.add(self.sen_stage1EN_tit) 
        self.stage1.titles.add(self.sen_stage1FR_tit) 
        
        self.sen_stage2EN_tit = Sentence.objects.create(text="Intermediate",language=self.l_TR,author=self.user)
        self.stage2 = Stage.objects.create(position=2,course=self.c1,author=self.user)
        self.stage2.titles.add(self.sen_stage2EN_tit) 
         
        self.e1 = Entry.objects.create(text="booklar",simplified="booklar",question=self.q1)
        self.a1 = Attempt.objects.create(user=self.user,entry=self.e1,assessment=Attempt.WRONG)
        self.a2 = Attempt.objects.create(user=self.user,entry=self.e1,assessment=Attempt.WRONG)
        self.a3 = Attempt.objects.create(user=self.u2,entry=self.e1,assessment=Attempt.CORRECT)
        self.a4 = Attempt.objects.create(user=self.u3,entry=self.e1,assessment=Attempt.REPORT,feedback="I think booklar is also right")
        return self
    
    def test_NoRegistrations_to_Registered_for_Course(self):
        self.client.login(username='joe', password='qweqweqwe')

        # check trying to access dashboard redirects to signup for courses
        response = self.client.get(reverse('Quiz:dashboard'),follow=True)
        self.assertRedirects(response,reverse('Quiz:signup-course'))

        # check content of signup page
        self.assertContains(response, self.c1.title_in(self.l_EN))
        self.assertContains(response, self.c2.title_in(self.l_EN))
        self.assertNotContains(response, self.c3.title_in(self.l_EN))
        # sign up for Learn Turkish
        response = self.client.post(reverse('Quiz:register-student-to-course'), {'choice': "{},{}".format(self.c1.id,self.l_EN.id)}, follow=True)
        self.assertRedirects(response,reverse('Quiz:gram-create'),status_code=302, target_status_code=200)
        self.sp = StudentProgress.objects.first()

        # simulate assessment
        self.sp.score = 3000
        self.sp.variance = StudentProgress.VARIANCE_CUTOFF_INITIAL_PLACEMENT
        self.sp.stage = self.stage1
        self.sp.initial_placement = False 
        self.sp.save()
        # after assessment
        # subscribed - dashboard shows correct output
        response = self.client.get(reverse('Quiz:dashboard'),follow=True)
#        self.assertRedirects(response,'',status_code=302,target_status_code=200)
        self.assertContains(response, self.sen_c1_tit)
        # attempt sign-up, confirm Learn Turkish is no longer shown
        response = self.client.get(reverse('Quiz:signup-course'))
        self.assertNotContains(response, self.sen_c1_tit)
        self.assertContains(response, self.sen_c2_tit)
        self.assertNotContains(response, self.sen_c3_tit)



    def test_Weekly_Assessment(self):
        self.test_NoRegistrations_to_Registered_for_Course()
        # start weekly assessment
        response = self.client.get(reverse('Quiz:assessment_init'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('Quiz:assessment_schedule_qs'))
#        while (response.url == reverse('Quiz:assessment_schedule_qs')):
#            response = self.client.get(response.url)
#        self.assertEqual(response.url, reverse('Quiz:assessment_ask_qs'))
        
        # confirm a question is posed

class AttemptTests(TestCase):
    pass
    def test_hardest_questions(self):
        # returns questions with the greatest percentage of Wrong self_assessments.
        pass
        
    
    def test_most_ambiguous_questions(self):
        # returns questions with the greatest percentage of Ambiguous self_assessments.
        pass
 
class CommentTests(TestCase):
    pass

class CourseTests(TestCase):
    pass
   
"""class EntryTests(TestCase):

#     def setUp(self):
#         t = ViewTests.setUp(self)
#         print('EntryTests.Setup called')
#         t = ViewTests.setUp(self)
#         #print (self)
        
    def test_unique_users_several(self):        
        t = ViewTests.setUp(self)
        print('EntryTests.test_unique_users_several called')
        self.assertEqual(self.e1.unique_users(), 3)
        
    def test_unique_users_none(self):
        t = ViewTests.setUp(self)
        print('EntryTests.test_unique_users_none called')
        pass

    def test_recurring_patterns(self):
        t = ViewTests.setUp(self)
        print('EntryTests.test_recurring_patterns called')
        pass
    
class GramConceptTests(TestCase):
    pass


class GrammarProgressTests(TestCase):
    pass

class GrammarTests(TestCase):
    pass

class LanguageTests(TestCase):
    pass

class QuestionTests(TestCase):
    pass

class SentenceTests(TestCase):
    pass

class StageTests(TestCase):
    pass
 
class StudentProgressTests(TestCase):
    pass
    
class StudentTests(TestCase):
    pass

"""