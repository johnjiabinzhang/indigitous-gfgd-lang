from django.db import models
from common import css
from .stage import Stage
from .question import Question
from .grammar_progress import GrammarProgress
from .grammar import Grammar
from .student import Student
from django.contrib import messages
from django.core.validators import validate_comma_separated_integer_list
from random import randint
from django.contrib.auth.models import User
from datetime import date
import logging

# Intermediate table betweeen user and course. Also tracks stage, but stage may be null.
# StudentProgress records a student's progress through a language learning course.
# This, combined with GrammarProgress sum up a student's progress through a language

class StudentProgress(models.Model):
    VARIANCE_MAX = 1000
    VARIANCE_CUTOFF_INITIAL_PLACEMENT = 400
    VARIANCE_MIN_REQUIRED  = 100
    
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    course = models.ForeignKey('Course', on_delete=models.CASCADE, db_index=True)
    native_language = models.ForeignKey('Language', on_delete=models.CASCADE) # ties the course's target language to a specific native language
    score = models.FloatField(default = 0)
    variance = models.FloatField(default = VARIANCE_MAX)
    stage = models.ForeignKey('Stage',blank=True,null=True)
    last_assessment = models.DateField(auto_now=True)
    scheduled_qs = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")
    focuses = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")
    initial_placement = models.BooleanField(default=True) # records if in initial_placement mode

    def lower_variance(self):
        self.variance = self.variance * 0.8
        self.save()

    def refresh_variance(self):
        # increase the variance based on estimated hours the student has studied since last assessment
        student = Student.objects.get(user=self.user)
        days_elapsed = (date.today() - self.last_assessment).days
        hours_studied = days_elapsed * student.hours_per_day
        self.variance = min (self.VARIANCE_MAX, self.variance + days_elapsed*47) # max variance of 1000, achieved if away for 21 days.
        self.last_assessment = date.today()
        self.save()

    def rescore(self,score):
        self.score = max(score, 0)
        self.save()        

    def save(self, *args, **kwargs):
        if self.stage is None:
            stage = self.course.lowest_stage()
        super(StudentProgress, self).save(*args, **kwargs)



    def select_nearest_ELO_q(self, asked_qs):
        # find the question closest to the user's current ELO for grammars related to the current course
        target_score = max(self.score + randint(int(-self.variance*1.5),int(self.variance*1.5)),0)
        nearest_grammar_ids = Grammar.objects.nearest_grammars(self.course,self.score).values_list('id', flat=True)
        possible_qs = Question.objects.filter(grammar__in=nearest_grammar_ids)
        shortened_list = possible_qs.exclude(id__in=asked_qs)
#        selected_q = shortened_list.filter().extra(select={"diff": "abs(score-%s)" % target_score}).order_by("diff").first()
        # since qs clump and we dont just want an outlier, we will randomly choose a q from the set remaining
        if shortened_list.count() > 0:
            random_index = randint(0, shortened_list.count() - 1)        
            selected_q = shortened_list.all()[random_index]
        else:
            selected_q = None 
        return selected_q
        
    def set_stage(self):
        # set the user's stage to that matching their ELO score, and generate the GrammarProgressions
        # start at bottom, work up through stages
        self.stage = None
        self.save()
        target_stage = self.determine_stage()
        while self.stage != target_stage:
            self.promote(stage_pos=target_stage.position)
        
    def determine_stage(self):
        return self.course.determine_stage(score=self.score)
        
    def promote(self,stage_pos=None):
        # if the user's score permits it, raises the user to the next stage and creates appropriate GrammarProgresses
        # Optional stage_pos will ensure GrammarProgress.past_results is correctly set 
        if self.stage is None:
            self.stage = self.course.lowest_stage()
        else:
            next_stage = Stage.objects.filter(position__gt=self.stage.position,course=self.course).order_by("position").first()
            if next_stage is None: 
                # the user has achieved the highest stage there is
                return None
            else:
                self.stage = next_stage 
        if stage_pos is None:
            stage_pos = self.stage.position
        for grammar in self.stage.grammar_set.all():
            if not GrammarProgress.objects.filter(sp=self,grammar=grammar).exists():
                gp = GrammarProgress.objects.create_gp(sp=self,grammar=grammar,stage_position=stage_pos)
        self.save()
        return self.stage

    def __str__(self):
        return str([self.user, self.course, self.stage])

    class Meta:
        verbose_name_plural = "Student Progressions"
        unique_together = ("user", "course")
