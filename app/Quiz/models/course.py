from django.db import models
from django.contrib.auth.models import User
from .language import Language
from .stage import Stage
import logging

# A course is defined as teaching target_language
# titles contains the course name in the native_languages that are adequately prepared for this course

class Course(models.Model):
    titles = models.ManyToManyField('Sentence',blank=True)
    target_language = models.ForeignKey('Language', related_name='target_courses', on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    last_edited = models.DateField(auto_now=True)

    def __str__(self):
        return (self.target_language.anglicized_name)

    def save(self, *args, **kwargs):
        super(Course, self).save(*args, **kwargs)
        if not self.stage_set.exists():
            Stage.objects.create(position=1,course=self,author=self.author)
        log = logging.getLogger(__name__)
        log.info(("Model Course Instance saved:"+str(self)).encode("utf-8"))

    def lowest_stage(self):
        return self.stage_set.order_by("position").first()

    def native_languages(self):
        # returns all native_languages this course is available in
        lang_ids = self.titles.values_list('language', flat=True)
        return Language.objects.filter(id__in=lang_ids)

    def title_in(self,language):
        title = self.titles.filter(language=language).first()
        if title is not None:
            title = title.text
        else:
            title = "NO_TITLE"      
        return title    

    def determine_stage(self,score):
        # Given an ELO score, returns the stage most appropriate (first stage with a score higher than 'score')
        # If the score is above all available stages, returns last registered stage
        return self.stage_above(score)

    def stage_above(self,score):
        last_stage = None
        for stage in Stage.objects.filter(course=self).order_by("position"):
            last_stage = stage
            if stage.score() > score:
                break
        return last_stage
        
    def stage_below(self,score):
        last_stage = None
        for stage in Stage.objects.filter(course=self).order_by("position"):
            if stage.score() > score:
                break
            last_stage = stage                
        return last_stage




