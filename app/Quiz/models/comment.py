from django.db import models
from django.contrib.auth.models import User
import logging

class Comment(models.Model):
    # A comment can be about a model instance of any model type, 
    # hence comment model does not have its own parent_model foreign key.
    
    text = models.TextField(help_text="Comment")
    language = models.ForeignKey('Language', null=True, on_delete=models.CASCADE)
    parent = models.ForeignKey('Comment', null=True, blank=True, on_delete=models.PROTECT, db_index=True, help_text="If this comment is in reply to a previous comment")
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    last_edited = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.text)

    def save(self, *args, **kwargs):
        super(Comment, self).save(*args, **kwargs)
