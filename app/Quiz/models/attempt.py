from django.db import models
from django.contrib.auth.models import User
from .entry import Entry
import logging

class AttemptManager(models.Manager):
    def q_type_of(self, target_type, results_desired=1, language=None):
        # returns questions with the greatest percentage of target_type self_assessments.
        
        q_scores = {} # dictionary to record highest scoring questions
#        Qs = Attempt.objects.order_by().values('question').distinct()    # could also do this operation on question_progress table, but attempt table keeps records even for deleted users
        for q in Qs:
            # skip Qs of another language
            if (language is not None):
                if q.question.grammar.stage.target_language != language:
                    continue # skip the rest of this iteration
            
            # find all attempts for this question
            wrong_count = 0
            As = Attempt.objects.filter(question = q)    
            for a in As:
                # iterate through each attempt and count wrongs
                if a.self_assesment == target_type:
                    wrong_count += 1
            q_scores[q] = wrong_count/len(As)
        
        
        results = []
        return_count = min(results_desired,len(q_scores))
        for i in range(return_count):
            r = max(q_scores, key=q_scores.get)
            results.append(r)
            del(q_scores[r])
        return results

    def hardest_questions(self, results_desired=1, language=None):
        return self.q_type_of(Attempt.WRONG,language,results_desired)
    
    def most_reported_questions(self, results_desired=1, language=None):
        return self.q_type_of(Attempt.REPORT,language,results_desired)

class Attempt(models.Model):
    # User attempt and results at a question
    # Records their result, points to an Entry related to what they typed, records the user, ELO and variance at time

    CORRECT = Entry.CORRECT
    WRONG = Entry.WRONG
    REPORT = Entry.REPORT
    A_TYPE_CHOICES = ((CORRECT, 'Right'), (WRONG, 'Wrong'), (REPORT, 'There is a problem with the question'))   

    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    raw_entry = models.CharField(max_length=200, help_text="entry text")
    entry = models.ForeignKey('Entry', on_delete=models.CASCADE)
    assessment = models.CharField(max_length=1,choices=A_TYPE_CHOICES, db_index=True)   
    feedback = models.CharField(max_length=200, help_text="What is it about the question that makes it unclear or misleading",blank=True)
    score = models.FloatField(null=True, blank=True, help_text="score of the user when this attempt was made - only recorded if variance below a certain threshold, otherwise null")    
    variance = models.FloatField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    #objects = AttemptManager()

    def save(self, *args, **kwargs):
        super(Attempt, self).save(*args, **kwargs)

    def __str__(self):
        return str([self.entry])