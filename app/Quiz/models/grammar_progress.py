from django.db import models
from django.contrib.auth.models import User
from .question import Question
from .attempt import Attempt
from django.core.validators import validate_comma_separated_integer_list
import collections
from common import css
from datetime import date
import logging

# Intermediate table between Grammar and StudentProgress
# Multiple GrammarProgressions and StudentProgress sum up a student's progress through
# a language learning course. While a student has an overall score recorded in StudentProgress
# they start each grammar with a zero score that must be built up. The score is based on the average
# of the user's past answers. A history of up to the last 20 attempts are remembered. Past_results
# begins with two Wrongs recorded so that two Rights are required to score 50% and 20 Rights to score 100%

class GrammarProgressManager(models.Manager):
    def create_gp(self, sp, grammar, stage_position):
        gap = stage_position - grammar.stage.position
        if gap>2:
            past_results=Attempt.WRONG
        elif gap==1:
            past_results=Attempt.WRONG*2
        else:
            past_results=Attempt.WRONG*3
        gp = self.create(sp=sp,grammar=grammar,past_results=past_results)
        return gp


class GrammarProgress(models.Model):    
    sp = models.ForeignKey('StudentProgress', on_delete=models.CASCADE, db_index=True)
    grammar = models.ForeignKey('Grammar', on_delete=models.CASCADE, db_index=True)
    asked_qs = models.TextField(blank=True,null=True,validators=[validate_comma_separated_integer_list],default="")
    past_results = models.CharField(max_length=100,default=Attempt.WRONG*2,help_text="Tracks past attempts to calculate GP progress")
    last_attempt_date = models.DateField(auto_now=True)
    started = models.BooleanField(default=False) # set true when add_q is run
    recent_error_count = models.IntegerField(default=0)
    objects = GrammarProgressManager()

    def passing(self,user_stage_position):
        # reports if the user presently holds a passing score for this grammar
        # as the user advances stages, lower grammars require a higher passing score resulting in having to
        # revisit previously passed grammars. The severity of increase is determined by "V", where I below show the 
        # passing mark for various V values to pass a grammar when it is the current stage, to what the pass mark
        # increases to after the user has advanced around 10 stages beyond: 
        # V=0.8 => Current stage pass mark:56%
        #        10 stages later pass mark:95%
        # V=0.6 => Current stage pass mark:62%
        #        10 stages later pass mark:96%
        V=0.8 
        pass_score = user_stage_position - self.grammar.stage.position + 1 / (user_stage_position - self.grammar.stage.position + 1 + V)  
        return self.get_score() > pass_score
        
    def add_q(self,question):
        # records that this question has been previously asked
        self.asked_qs = css.add(self.asked_qs,question.id)
        self.started = True
        self.save()
    
    def recently_failed(self):
        # reports if there have been two or more fails at this grammar in the last 24-48 hours
        days_elapsed = (date.today() - self.last_attempt_date).days
        return (days_elapsed <= 1 and self.recent_error_count >= 2)
        
    def get_asked(self):
        # returns a list of tuples sorted from least common asked question to most common (for asked qs)
        asked_qs = css.to_list(self.asked_qs)
        frequency_counted_qs = collections.Counter(asked_qs)
        try:
            return (list(frequency_counted_qs.keys()), frequency_counted_qs.most_common()[-1][0])
        except:
            return ([], None)
        
    def get_unasked(self):
        # returns all questions that have never been asked for this grammar
        return Question.objects.filter(grammar=self.grammar).exclude(id__in=self.get_asked())

    def select_question(self):
        # provides question for this grammar never asked, otherwise least asked
        possible_qs = self.grammar.question_set.all()
        asked_qs,least_asked = self.get_asked()
        unasked_qs = possible_qs.exclude(id__in=asked_qs)
        if unasked_qs.exists():
            selected_q = unasked_qs.best_quality()
        else:
            selected_q = Question.objects.get(id=least_asked)
        self.add_q(selected_q)
        return selected_q

    def add_score(self,score):
        # Adds the latest attempt score to past_results
        # score can be accessed through self.get_score()
        VALID_SCORES = {Attempt.WRONG, Attempt.CORRECT, Attempt.REPORT}
        if score not in VALID_SCORES:
            raise ValueError('%c is not a valid score value - choose from %s' % (score,VALID_SCORES))        
        if len(self.past_results)>10:
            self.past_results=self.past_results[:1] #lose the oldest entry
        self.past_results += score
        days_elapsed = (date.today() - self.last_attempt_date).days
        if days_elapsed > 1:
            if score == Attempt.WRONG:
                recent_error_count = 1
            else:
                recent_error_count = 0
        elif score== Attempt.WRONG:
            self.recent_error_count += 1
        self.save()        
        return self.recent_error_count

    def get_score(self):
        wrong_attempts=self.past_results.count(Attempt.WRONG)
        right_attempts=self.past_results.count(Attempt.CORRECT)
        total_attempts = len(self.past_results)
        if total_attempts == 0:
            return -1
        else:
            return right_attempts/total_attempts
        
    def __str__(self):
        return str([self.sp.user, self.grammar, str(self.get_score()*100)+"%"])


    def save(self, *args, **kwargs):
        super(GrammarProgress, self).save(*args, **kwargs)
     
    class Meta:
        verbose_name_plural = "Grammar Progressions"
        unique_together = ("sp", "grammar")
        order_with_respect_to = "grammar"