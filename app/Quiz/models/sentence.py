from django.db import models
from django.contrib.auth.models import User
import logging

# Sentences are a simple sentence writting in a particular language. They provide
# the building blocks for questions. As it will be useful to translate a question
# into multiple languages, given that translation is an information lossy process it is
# important to identify the source sentence to a group of related translation sentences.
# For this reason, a source field is provided. It provides the primary key of the original
# source sentence for which this is a translation. "0" indicates this itself is an 
# original source sentence.
# A sentence can consist of multiple grammars.
# A sentence records its last_edited date, useful for determining whether or not Complaints 
# on sentences are still valid.

class Sentence(models.Model):
    text = models.CharField(max_length=200, help_text="Sentence text")
    language = models.ForeignKey('Language', related_name='language_sentences', help_text="Language this is written in")
    parent = models.ForeignKey('Sentence', null=True, blank=True, help_text="Points to the sentence this is translated from. Null indicates this is a source sentence.")    
    grammars = models.ManyToManyField('Grammar', blank=True)
    last_edited = models.DateField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    #vocabs = models.ManyToManyField(Vocab)
    
    def __str__(self):
        return str(self.text)

    def save(self, *args, **kwargs):
        super(Sentence, self).save(*args, **kwargs)

#    class Meta:
    
