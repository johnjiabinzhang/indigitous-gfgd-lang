from django.db import models
from django.contrib.auth.models import User
from django.db.models import Avg, Q
from .stage import Stage

import logging
    
""" 
    Grammar is an instance of a GrammarConcept specific to a particular target_language
    (there may be multiple Grammars expressing a specific GrammarConcept in that target_language)

    explanations provide teaching explanations of that Grammar for different native_languages
    example_questions illustrating the grammar for different native_languages. 
    score reflects the difficulty of the grammar in relation to others for its stage, it also
    provides a starting score for all newly added questions indexed to this grammar. 
"""
class GrammarManager(models.Manager):
    def nearest_grammars(self,course,score):
        # returns all grammars for stage on either side of score
        lower_stage = course.stage_below(score)
        upper_stage = course.stage_above(score)
        return super(GrammarManager, self).get_queryset().filter(Q(stage=lower_stage)|Q(stage=upper_stage))


class Grammar(models.Model):
    stage = models.ForeignKey('Stage', null=True, blank=True, db_index=True)
    gram_concept = models.ForeignKey('GramConcept', help_text="The grammar concept for which this is providing a translation")
    target_language = models.ForeignKey('Language', related_name='target_grammars', help_text="The target language this grammar is giving an example")
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    titles = models.ManyToManyField('Sentence',blank=True)
    explanations = models.ManyToManyField('Comment',help_text="teaching explanation on this grammar written in native language")
    example_questions = models.ManyToManyField('Question', related_name='grammar_example_questions', help_text="A question that is a good representative example of this grammar")
    score = models.IntegerField(null=True,blank=True,db_index=True,help_text="Grammars have an ELO score that increases further as you work through a stage, hence indicating order")
    old_score = models.FloatField(blank=True, default=0)
    last_edited = models.DateField(auto_now=True)

    objects = GrammarManager()

    def full_title(self,language):
        return self.title_in(language) + "  (" + self.gram_concept.title + ")"

    def title_in(self,language):
        title = self.titles.filter(language=language).first()
        if title is not None:
            title = title.text     
        else:
            title = "NO_TITLE"    
        return str(title)

    def explanation_in(self,language):
        explanation = self.explanations.filter(language=language).first()
        if explanation is not None:
            explanation = explanation.text        
        return str(explanation)

    def save(self, *args, **kwargs):
        # Primarily this is to be called when the instance is initially created. setting the inital score value.
        if self.score is None:
            if self.stage is not None:
                self.score = self.stage.score()
                self.old_score = self.score
        elif self.score != self.old_score:
            old_score = self.old_score
            self.old_score = self.score
            for question in self.question_set.all():
                # modify question scores - work out average of scores resave score with offset.
                offset = question.score - old_score
                question.score = self.score + offset
                question.save()
        super(Grammar, self).save(*args, **kwargs)

    def __str__(self):
        title = self.titles.first()
        if title is not None:
            title = title.text
        return str(title)


    def rescore(self):
        # rescores the grammar to be average of its question ELOs
        old_score = self.score
        self.score = Question.objects.filter(grammar = self.id).aggregate(Avg('score'))         # get average score of all questions
        self.save()        
        log = logging.getLogger(__name__)
        log.info("Model Grammar rescored from "+old_score+"->"+self.score)

    class Meta:
        order_with_respect_to = 'score'